<?php 

include APP_PATH . '/src/helpers/count_functions.php';
require_once (APP_PATH. '/src/controllers/MainController.php');
?>	


<!DOCTYPE html>
<html>
<head>
	<title>Dashboard</title>
	<link rel="stylesheet" type="text/css" href="css/dashboard.css">
</head>
<body>

      <div class="wrapper">
 		
 		<div class="dash_section">
 			<ul><li><a>Dashboard</a></li>
 				<li><a>Utilizatori</a></li>
 				<li><a>Articole</a></li>
 			   <a href="index.php?action=main">Logout</a>  
      </ul>
 		
 		 
        
        </div>

       <div class="users_count">
       	<h4>Utilizatori</h4>
        <span><?php echo count_users();?></span>
       </div>
      
       <div class="editors_count">
       	<h4>Editori</h4>
       	 <span><?php echo count_editors();?></span> 
       </div>
       
      <div class="articles_count">
       	<h4>Postari</h4>  
         <span><?php echo count_articles();?></span> 
     </div>


    </div>

</body>
</html>